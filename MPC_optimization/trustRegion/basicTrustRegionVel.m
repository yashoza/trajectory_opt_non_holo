function     [ trustDelta,constVelocity,oldConstraint,newConstraint,mu,nu ] = basicTrustRegionVel(constVelocity,steps,CVXVel,deltaT,guess,initialX,initialY,initialAng,initialOmega,finalX,finalY,finalAng,obstX,obstY,radiusObst,trustDelta,mu,nu,prev_optval,cvx_optval,tau)

%BASICTRUSTREGION Summary of this function goes here
%   Detailed explanation goes here

prevGuess_vel = constVelocity;
constVelocity = double(CVXVel(1:steps-1,:));

[~,~,~,~,header_new,const_x_new,const_y_new] = stateMatrix(steps,guess,deltaT,prevGuess_vel,initialX,initialY,initialAng,initialOmega);
[~,~,~,~,header_old,const_x,const_y] = stateMatrix(steps,guess,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);


numer = 0;
oldConstraint = cell(size(obstX,2),1);
newConstraint = cell(size(obstX,2),1);
for v = 1:size(obstX,2)
    new_obst_lil = ((const_x_new(2:end,:)-obstX(:,v)).^2 + (const_y_new(2:end,:)-obstY(:,v)).^2) - (radiusObst(:,v) + tau).^2;
    old_obst_lil = ((const_x(2:end,:)-obstX(:,v)).^2 + (const_y(2:end,:)-obstY(:,v)).^2) - (radiusObst(:,v) + tau).^2;
    oldConstraint{v,:} = old_obst_lil;
    newConstraint{v,:} = new_obst_lil;
    numer = numer;
end
numer = numer + ((const_x_new(end,:)-finalX).^2 + (const_y_new(end,:)-finalY).^2 + (header_new(end,:)-finalAng).^2) - ((const_x(end,:)-finalX).^2 + (const_y(end,:)-finalY).^2 + (header_old(end,:)-finalAng).^2);

denom = (cvx_optval - prev_optval - sum(guess.^2));

rho = numer/denom;
if rho > 0
    mu = mu*max(1/3,1-(2*rho - 1)^3);
    nu = 2;
else
    mu = mu*nu;
    nu = 2*nu;
end


