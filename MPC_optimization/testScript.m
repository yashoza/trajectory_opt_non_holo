clear;
clc;

figure(1);
cla;
axis([-5,55,-5,55]);
hold on;

rectX = [0,50,50,0,0];
rectY = [0,0,50,50,0];
plot(rectX,rectY,'b');

line1X = [10,40];
line1Y = [15,15];
plot(line1X,line1Y,'b');

line2X = [10,40];
line2Y = [35,35];
plot(line2X,line2Y,'b');

for i = 15:5:35
    plot([i,i-2],[15,10],'b');
    plot([i,i-2],[15,20],'b')
end

for i = 15:5:35
    plot([i,i-2],[35,30],'b');
    plot([i,i-2],[35,40],'b')
end

for i = 10:5:40
    plot([50,45],[i,i-2],'b');
end