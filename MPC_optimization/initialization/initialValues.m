function [steps,deltaT,initialX,initialY,initialAng,initialOmega,finalX,finalY,finalAng,finalOmega,obstX,obstY,radiusObst,numberObst,guess,constVelocity,cvxIterations,penalty,lambda,trustEta,trustDelta,trustDeltaMax,prev_optval_alpha,prev_optval_vel,alpha_max,headerd_max,mu,nu,mainRadius,tau,vel_max,accel_max,lambda_vel,penalty_vel,initialVel,finalVel,radMin,obstth,obstLen,obstWid,len,wid,circlesorpolygons] = initialValues()
%INITIALVALUES Initializes all values needed for optimization
%   Detailed explanation goes here

% Total time to plan and number of steps to decide deltat
to = 0;
tf = 40;
steps = 50;
temp_time = linspace(to,tf,steps);
deltaT = temp_time(:,2)-temp_time(:,1);

% decide now please
circlesorpolygons = 1;

% Initial conditions for robot
initialX = -5;
initialY = -5;
initialAng = 0;
initialOmega = 0;
initialVel = 0;

% Final conditions for robot
finalX = 5;
finalY = 5;
finalAng = 0;
finalOmega = 0;
finalVel = 0;

% maxconstraints
alpha_max = 1;
headerd_max = 0.8;

% main radius
mainRadius = 2;

% maximums
vel_max = 1;
accel_max = 0.5;

% ego dimensions

len = 1;
wid = 0.6;

if(circlesorpolygons == 0)
    
    % Obstacles conditions
    obstX = [0];
    obstY = [0];
    radiusObst = [0.2];
    % obstX = [10,12,10.06,13.01,5.686,9.969]-3;
    % obstY = [4,10,7.878,3.213,1.273,1.584]-1;
    % radiusObst = [1,0.8,0.7,0.5,1,0.7];
    % butterCup = 12;
    % obstX = randi([5 11],1,butterCup);
    % obstY = randi([5 11],1,butterCup);
    % minR = 0.2;
    % maxR = 0.65;
    % radiusObst = mainRadius+((maxR-minR)*abs(rand(1,butterCup))) + minR;
    numberObst = size(obstX,2);
    
    % dynamic obstacles
%     butterCup = 5;
%     minVel = 0;
%     maxVel  = 0.1;
%     velObst = ((maxVel-minVel)*rand(1,butterCup)) + minVel;
%     minAng = pi/4;
%     maxAng = 1.76*pi;
%     olpoth = ((maxAng-minAng)*rand(1,butterCup)) + minAng;
%     obstDiffVelX = velObst.*cos(olpoth);
%     obstDiffVelY = velObst.*sin(olpoth);
%     obstPosX = randi([-3 3],1,butterCup);
%     obstPosY = randi([-3 3],1,butterCup);
%     blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)];
%     blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)];
%     fixX = cumsum(blissPosX,1);
%     fixY = cumsum(blissPosY,1);
%     obstX = fixX(2:end,:);
%     obstY = fixY(2:end,:);
%     minR = 0.2;
%     maxR = 0.6;
%     radiusObst = mainRadius+((maxR-minR)*abs(rand(1,butterCup))) + minR;
%     numberObst = size(obstX,2);
    
    % antipodal obstacles
    % dolly = sqrt((finalY-initialY)^2 + (finalX-initialX)^2);
    % velObst = (dolly)/(tf-to);
    % goli = 5/sqrt(2);
    % obstPosX = [0,0,goli,goli,-goli,-goli];
    % obstPosY = [5,-5,goli,-goli,-goli,goli];
    % angObst = [-pi/2,pi/2,-(3*pi)/4,(3*pi)/4,(pi)/4,-(pi)/4];
    % obstDiffVelX = velObst.*cos(angObst);
    % obstDiffVelY = velObst.*sin(angObst);
    % blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)*deltaT];
    % blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)*deltaT];
    % fixX = cumsum(blissPosX,1);
    % fixY = cumsum(blissPosY,1);
    % obstX = fixX(2:end,:);
    % obstY = fixY(2:end,:);
    % minR = 0.2;
    % maxR = 0.9;
    % radiusObst = mainRadius+((maxR-minR)*abs(rand(1,6))) + minR;
    % numberObst = size(obstX,2);
    obstth = zeros(size(obstX));
    obstLen = zeros(size(obstX,2));
    obstWid = zeros(size(obstX,2));
else
    
    % nothing
    %
%     obstX = [];
%     obstY = [];
%     radiusObst = [];
%     numberObst = size(obstX,2);
%     obstth = [];
%     obstLen = [];
%     obstWid = [];
%     
    % testcase 1
%     velObst = [0];
%     olpoth = [0];
%     obstth = repmat(olpoth,steps-1,1);
%     obstDiffVelX = velObst.*cos(olpoth)*deltaT;
%     obstDiffVelY = velObst.*sin(olpoth)*deltaT;
%     obstPosX = [4];
%     obstPosY = [-5];
%     blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)];
%     blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)];
%     fixX = cumsum(blissPosX,1);
%     fixY = cumsum(blissPosY,1);
%     obstX = fixX(2:end,:);
%     obstY = fixY(2:end,:);
%     radiusObst = zeros(steps-1,1);
%     numberObst = size(obstX,2);
%     obstLen = [0.9];
%     obstWid = [0.7];
    

%     velObst = [0,0,0,0,0.05];
%     olpoth = [0,0,0,0,0];
%     obstth = repmat(olpoth,steps-1,1);
%     obstDiffVelX = velObst.*cos(olpoth)*deltaT;
%     obstDiffVelY = velObst.*sin(olpoth)*deltaT;
%     obstPosX = [-6,-6,6,6,1];
%     obstPosY = [6,-6,6,-6,0.25];
%     blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)];
%     blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)];
%     fixX = cumsum(blissPosX,1);
%     fixY = cumsum(blissPosY,1);
%     obstX = fixX(2:end,:);
%     obstY = fixY(2:end,:);
%     radiusObst = zeros(steps-1,3);
%     numberObst = size(obstX,2);
%     obstLen = [10,14,14,10,1];
%     obstWid = [11,7,7,11,0.6];
% 


    velObst = [0];
    olpoth = [pi/4];
    obstth = repmat(olpoth,steps-1,1);
    obstDiffVelX = velObst.*cos(olpoth)*deltaT;
    obstDiffVelY = velObst.*sin(olpoth)*deltaT;
    obstPosX = [2];
    obstPosY = [0];
    blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)];
    blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)];
    fixX = cumsum(blissPosX,1);
    fixY = cumsum(blissPosY,1);
    obstX = fixX(2:end,:);
    obstY = fixY(2:end,:);
    radiusObst = zeros(steps-1,3);
    numberObst = size(obstX,2);
    obstLen = [3];
    obstWid = [4];



    %     Static simpleload
%     obstX =0*ones(steps-1,1);
%     obstY =0*ones(steps-1,1);
%     radiusObst = 1*ones(steps-1,1);
%     numberObst = size(obstX,2);
%     obstth = (-pi/2)*ones(steps-1,1);
%     obstLen = [1.0];
%     obstWid = [0.7];
    
    %     % dynamic cmoplex
%     butterCup = 5;
%     minVel = 0;
%     maxVel  = 0.1;
%     velObst = ((maxVel-minVel)*rand(1,butterCup)) + minVel;
%     minAng = pi/4;
%     maxAng = 1.76*pi;
%     olpoth = ((maxAng-minAng)*rand(1,butterCup)) + minAng;
%     obstth = repmat(olpoth,steps-1,1);
%     obstDiffVelX = velObst.*cos(olpoth);
%     obstDiffVelY = velObst.*sin(olpoth);
%     obstPosX = randi([-3 5],1,butterCup);
%     obstPosY = randi([-3 5],1,butterCup);
%     blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)];
%     blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)];
%     fixX = cumsum(blissPosX,1);
%     fixY = cumsum(blissPosY,1);
%     obstX = fixX(2:end,:);
%     obstY = fixY(2:end,:);
%     radiusObst = zeros(size(obstX));
%     minLength = 0.8;
%     maxLength = 1.5;
%     obstLen = ((maxLength-minLength)*rand(1,butterCup)) + minLength;
%     obstWid = ((maxLength-minLength)*rand(1,butterCup)) + minLength;
%     numberObst = size(obstX,2);
    
    
    %     antipodal obstacles
%     dolly = sqrt((finalY-initialY)^2 + (finalX-initialX)^2);
%     velObst = (dolly)/(tf-to);
%     goli = 5/sqrt(2);
%     obstPosX = [0,0,goli,goli,-goli,-goli];
%     obstPosY = [5,-5,goli,-goli,-goli,goli];
%     palpo = [-pi/2,pi/2,-(3*pi)/4,(3*pi)/4,(pi)/4,-(pi)/4];
%     obstth = repmat(palpo,steps-1,1);
%     obstDiffVelX = velObst.*cos(palpo);
%     obstDiffVelY = velObst.*sin(palpo);
%     blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)*deltaT];
%     blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)*deltaT];
%     fixX = cumsum(blissPosX,1);
%     fixY = cumsum(blissPosY,1);
%     obstX = fixX(2:end,:);
%     obstY = fixY(2:end,:);
%     minLength = 0.5;
%     maxLength = 0.9;
%     obstLen = ((maxLength-minLength)*rand(1,6)) + minLength;
%     obstWid = ((maxLength-minLength)*rand(1,6)) + minLength;
%     radiusObst = zeros(size(obstX));
%     numberObst = size(obstX,2);
%     
%         road senario
%             velObst1 = 0.1433*ones(13,1);
%             velObst2 = 0.0993*ones(36,1);
%             velObst = [velObst1;velObst2];
%             obstPosX = [-1.5];
%             obstPosY = [0];
%             palpo = [0];
%             obstth = repmat(palpo,steps-1,1);
%             obstDiffVelX = velObst.*cos(obstth);
%             obstDiffVelY = velObst.*sin(obstth);
%             blissPosX = [obstPosX;obstDiffVelX*deltaT];
%             blissPosY = [obstPosY;obstDiffVelY*deltaT];
%             fixX = cumsum(blissPosX,1);
%             fixY = cumsum(blissPosY,1);
%             obstX = fixX(2:end,:);
%             obstY = fixY(2:end,:);
%             obstLen = 0.8;
%             obstWid = 0.5;
%             radiusObst = zeros(size(obstX));
%             numberObst = size(obstX,2);
    
end

% Initial Guess for alpha along path
guess = zeros(steps-1,1)/100;

% Heuristic for velocity along path
constVelocity = 0.1*ones(steps-1,1);
% standards:
% 0 deg - 0.376
% 45 deg - 0.35
% -45 deg - 0.4015
% 90 deg - 0.42

% number of iterations
cvxIterations = 70;

% penalty for augmented legrangian and
% lambda for lagrangian
lambda = ones((size(obstX,2))*(steps-1),1);
penalty = 50*ones((size(obstX,2))*(steps-1),1);
lambda_vel = ones((size(obstX,2))*(steps-1),1);
penalty_vel = 50*ones((size(obstX,2))*(steps-1),1);

% initilization for trust region
trustEta = abs(rand(1))/4;
trustDeltaMax = 0.3;
trustDelta = 0.03;

% initial optval
prev_optval_alpha = 0;
prev_optval_vel = 0;

% mu and nu
mu = 1000;
nu = 2;

% % clearence term
% multiplier = 0.005;
% m = 0:1:steps-1;
% tau = multiplier*m(2:end)';
tau = zeros(steps-1,1);

% min turn radius
radMin = 1;
end
