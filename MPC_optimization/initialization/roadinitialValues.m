function [steps,deltaT,initialX,initialY,initialAng,initialOmega,finalX,finalY,finalAng,finalOmega,obstX,obstY,radiusObst,numberObst,guess,constVelocity,cvxIterations,penalty,lambda,trustEta,trustDelta,trustDeltaMax,prev_optval_alpha,prev_optval_vel,alpha_max,headerd_max,mu,nu,mainRadius,tau,vel_max,accel_max,lambda_vel,penalty_vel,initialVel,finalVel,radMin,obstth,obstLen,obstWid,len,wid,circlesorpolygons] = initialValues()

to = 0;
tf = 40;
steps = 50;
temp_time = linspace(to,tf,steps);
deltaT = temp_time(:,2)-temp_time(:,1);

% decide now please
circlesorpolygons = 1;

% Initial conditions for robot
initialX = 0.75;
initialY = 0;
initialAng = pi/2;
initialOmega = 0;
initialVel = 0;

% Final conditions for robot
finalX = -4;
finalY = 6.25;
finalAng = pi;
finalOmega = 0;
finalVel = 0;

% maxconstraints
alpha_max = 0.1;
headerd_max = 0.2;

% main radius
mainRadius = 0.3;

% maximums
vel_max = 1;
accel_max = 0.1;

% ego dimensions

len = 1;
wid = 0.6;

if(circlesorpolygons == 0)
else
    % nothing
%     obstX = [];
%     obstY = [];
%     radiusObst = [];
%     numberObst = size(obstX,2);
%     obstth = [];
%     obstLen = [];
%     obstWid = [];
    
    %     Static simple
%     obstX = [1.775*ones(steps-1,1),0.15*ones(steps-1,1),-1.7*ones(steps-1,1)];
%     obstY = [0.02*ones(steps-1,1),0.49*ones(steps-1,1),1.39*ones(steps-1,1)];
%     radiusObst = [1*ones(steps-1,1),1*ones(steps-1,1),1*ones(steps-1,1)];
%     numberObst = size(obstX,2);
%     obstth = zeros(steps-1,3);
%     obstLen = [0.8,0.8,0.8];
%     obstWid = [1.1,1.1,1.1];
    
    %     road senario
    velObst = 0;
    obstPosX = [-5,5,0];
    obstPosY = [0,0,9];
    palpo = [pi,pi,0];
    obstth = repmat(palpo,steps-1,1);
    obstDiffVelX = velObst.*cos(palpo);
    obstDiffVelY = velObst.*sin(palpo);
    blissPosX = [obstPosX;repmat(obstDiffVelX,steps-1,1)*deltaT];
    blissPosY = [obstPosY;repmat(obstDiffVelY,steps-1,1)*deltaT];
    fixX = cumsum(blissPosX,1);
    fixY = cumsum(blissPosY,1);
    obstX = fixX(2:end,:);
    obstY = fixY(2:end,:);
    obstLen = [7,7,12];
    obstWid = [8,8,4];
    radiusObst = zeros(size(obstX));
    numberObst = size(obstX,2);
    
end

% Initial Guess for alpha along path
guess = zeros(steps-1,1);

% Heuristic for velocity along path
constVelocity = 0.1*ones(steps-1,1);
% standards:
% 0 deg - 0.376
% 45 deg - 0.35
% -45 deg - 0.4015
% 90 deg - 0.42

% number of iterations
cvxIterations = 70;

% penalty for augmented legrangian and
% lambda for lagrangian
lambda = ones((size(obstX,2))*(steps-1),1);
penalty = 50*ones((size(obstX,2))*(steps-1),1);
lambda_vel = ones((size(obstX,2))*(steps-1),1);
penalty_vel = 50*ones((size(obstX,2))*(steps-1),1);

% initilization for trust region
trustEta = abs(rand(1))/4;
trustDeltaMax = 0.3;
trustDelta = 0.03;

% initial optval
prev_optval_alpha = 0;
prev_optval_vel = 0;

% mu and nu
mu = 1000;
nu = 2;

% % clearence term
% multiplier = 0.005;
% m = 0:1:steps-1;
% tau = multiplier*m(2:end)';
tau = zeros(steps-1,1);

% min turn radius
radMin = 6;

save previouscase.mat
end

