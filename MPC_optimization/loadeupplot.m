
figure('Name','with out vel trust region','NumberTitle','off');
subplot(2,2,1);
plot(constVelocity,'r');
title('velocity');
xlabel('steps');
ylabel('Velocity - m/s');

subplot(2,2,2);
plot(headerd,'r');
title('angular velocity');
xlabel('steps');
ylabel('Angular velocity - rad/s');

subplot(2,2,3);
plot(guess,'r');
title('angular accelration');
xlabel('steps');
ylabel('Angular accelration - rad/s^2');

subplot(2,2,4);
plot(guess./constVelocity,'r');
title('curvature');
xlabel('steps');
ylabel('Curvature - 1/m');
