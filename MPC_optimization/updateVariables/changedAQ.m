function [ bigA,bigQ,bigC] = changedAQ( steps,A,Q,C,tempA,tempB,lambda,penalty,numberObst)
%CHANGEDAQ Summary of this function goes here
%   Detailed explanation goes here
if(numberObst >= 1)
    Do = lambda'*tempA';
    Di = -1*lambda'*tempB;
    LQ = -1*lambda';
    bigQ = [(Q'+Do');LQ'];
    bigC = C + Di;
    
    u = sqrt(penalty/2);
    blissF = tempA;
    blissC = tempB.*u;
    blissB = diag(u);
    for g = 1:steps-1
        blissF(g,:) = tempA(g,:).*u';
    end
    blissA = blissF';
    bigA = [A+(blissA'*blissA),-1*blissA'*blissB;-1*blissB'*blissA,blissB'*blissB];
    bigQ = bigQ + [-2*blissA'*blissC;2*blissB'*blissC];
    bigC = bigC + (blissC'*blissC);
else
    bigA = A;
    bigQ = Q';
    bigC = C;
end

