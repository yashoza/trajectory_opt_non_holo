% metrics

[~,~,~,~,~,const_x,const_y] = stateMatrix(steps,guess,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);

fprintf('Iterations : %4.0f\n',mainiter);
arc_length = 0;
for i = 1:steps-1
    arc_length = arc_length + sqrt((const_x(i+1)-const_x(i)).^2 + (const_y(i+1)-const_y(i)).^2);
end
fprintf('Arc length of path is : %4.4f\n',arc_length);

d0 = -1*ones(steps-2,1);
d1 = ones(steps-3,1);
fndfA = diag(d0) + diag(d1,1);
fndfB = [fndfA,zeros(steps-2,1)];
fndfB(end,end) = 1;
fndfC = (fndfB'*fndfB)/(deltaT^2);
first_order = constVelocity'*fndfC*constVelocity;
fprintf('First order Cost : %4.6f\n',first_order);
d0 = ones(steps-3,1);
d1 = -2*ones(steps-4,1);
d2 = ones(steps-5,1);
fndfA = diag(d0) + diag(d1,1) + diag(d2,2);
fndfB = [fndfA,zeros(steps-3,1),zeros(steps-3,1)];
fndfB(end,end) = 1;
fndfB(end,end-1) = -2;
fndfB(end-1,end-1) = 1;
fndfC = (fndfB'*fndfB)/(deltaT^4);
second_order = constVelocity'*fndfC*constVelocity;
fprintf('Second order Cost : %4.6f\n',second_order);

fprintf('alpha square cost : %4.6f\n',sum(guess.^2));