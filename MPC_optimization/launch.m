
function [mainiter] = launch(trust_val)
% initilizations
[steps,deltaT,initialX,initialY,initialAng,initialOmega,finalX,finalY,finalAng,finalOmega,obstX,obstY,radiusObst,numberObst,guess,constVelocity,cvxIterations,penalty,lambda,trustEta,trustDelta,trustDeltaMax,prev_optval_alpha,prev_optval_vel,alpha_max,headerd_max,mu,nu,mainRadius,tau,vel_max,accel_max,lambda_vel,penalty_vel,initialVel,finalVel,radMin,obstth,obstLen,obstWid,len,wid,circlesorpolygons] = initialValues();
% load('previouscase.mat')

mainiter = 1;
nimbu = true;
counter_alpha = 0;
counter_vel = 0;
fool_alpha = true;
fool_vel = true;
flagger = 0;

tic
while(nimbu)
    %     if(mainiter >= 3)
    %         fool_alpha = false;
    %     end
    if(fool_alpha == true)
        [half_mat,rev_half_mat,sin_total_vel,cos_total_vel,const_th,const_x,const_y] = stateMatrix(steps,guess,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);
        if(mainiter == 1)
            if (circlesorpolygons == 0)
                temp_obstX = obstX;
                temp_obstY = obstY;
            else
                [radiusObst,temp_obstX,temp_obstY] = latradiusOutput(steps,obstX,obstY,obstth,obstLen,obstWid,const_x,const_y,const_th,len,wid,initialX,initialY,finalX,finalY);
            end
        end
        [ A,Q,C,coef_x,coef_y ] = linearCost_Jsmooth_and_Jend( steps,deltaT,guess,initialAng,initialOmega,finalAng,half_mat,rev_half_mat,sin_total_vel,cos_total_vel,const_x,const_y,finalX,finalY );
        [ tempA,tempB ] = augmentedLagrangian( steps,const_x,const_y,coef_x,coef_y,temp_obstX,temp_obstY,radiusObst,guess,tau );
        [ bigA,bigQ,bigC] = changedAQ( steps,A,Q,C,tempA,tempB,lambda,penalty,numberObst);
        cvx_begin  quiet
        variables dummy_plot_alpha((numberObst+1)*(steps-1),1);
        headerdl = cumsum([initialOmega;dummy_plot_alpha(1:steps-1,:)*deltaT]);
        headerd = headerdl(2:end,:);
        minimize (((dummy_plot_alpha'*bigA*dummy_plot_alpha) + bigQ'*dummy_plot_alpha)+bigC)
        subject to
        % constraints
        alpha_max >= dummy_plot_alpha(1:steps-1,:);
        dummy_plot_alpha(1:steps-1,:) >= -1*alpha_max;
        trust_val + guess >= dummy_plot_alpha(1:steps-1,:);
        dummy_plot_alpha(1:steps-1,:) >= guess - trust_val;
        headerd_max >= headerd;
        headerd >= -1*headerd_max;
        (constVelocity/radMin) >= headerd;
        headerd >= -1*(constVelocity/radMin);
        dummy_plot_alpha(steps:end,:) >= 0;
        cvx_end
        toc
        [ trustDelta,guess,~,newConstraint,mu,nu,prevGuess ] = basicTrustRegion(guess,steps,dummy_plot_alpha,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega,finalX,finalY,finalAng,temp_obstX,temp_obstY,radiusObst,trustDelta,mu,nu,prev_optval_alpha,cvx_optval,tau);
        mixed_alpha = 0.01;
        if(numberObst >= 1)
            forceIndia = ((sum(repmat(double(dummy_plot_alpha(1:steps-1,:)),1,numberObst*(steps-1)).*tempA ,1))' - tempB - (dummy_plot_alpha(steps:end,:)));
            lambda = lambda+ penalty.*forceIndia;
            for p = 1:numberObst
                if ((min(newConstraint{p,:}) < 0) && (abs(min(newConstraint{p,:}))) > 0.01)
                    %                 if(penalty(((p-1)*(steps-1))+1,:) <50000000)
                    penalty(((p-1)*(steps-1))+1:((p)*(steps-1)),:) = penalty(((p-1)*(steps-1))+1:((p)*(steps-1)),:)*10;
                    %                 end
                else
                    % do nothing
                end
                if((abs(min(newConstraint{p,:}))) > mixed_alpha && (min(newConstraint{p,:}))<0)
                    mixed_alpha = (abs(min(newConstraint{p,:})));
                end
            end
        end
        if(abs(cvx_optval - prev_optval_alpha) <= 5*10^-3 && mixed_alpha < 0.011)
            counter_alpha = counter_alpha +1;
        else
            counter_alpha = 0;
        end
        prev_optval_alpha = cvx_optval;
        % debug
        disp('CVX_optval_alpha')
        disp(double(cvx_optval));
        disp('Status')
        disp(cvx_status);
        % debug
        %     cla;
        %     trigger = 0;
        %   static senario
        %     plotOtimalPath( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger);
    else
        counter_alpha = 1;
    end
    
    if(fool_vel == true)
        [half_mat,rev_half_mat,sin_total_vel,cos_total_vel,const_th,const_x,const_y] = stateMatrix(steps,guess,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);
        if (circlesorpolygons == 0)
            temp_obstX = obstX;
            temp_obstY = obstY;
        else
            [radiusObst,temp_obstX,temp_obstY] = latradiusOutput(steps,obstX,obstY,obstth,obstLen,obstWid,const_x,const_y,const_th,len,wid,initialX,initialY,finalX,finalY);
        end
        [ A,Q,C,coef_x,coef_y ] = linearCost_Jsmooth_and_Jend_velocity( steps,sin_total_vel,cos_total_vel,initialX,initialY,finalX,finalY,constVelocity,initialVel,finalVel,deltaT );
        A = A + (1*10^-10)*eye(steps-1);
        [ tempA,tempB ] = augmentedLagrangianVelocity( steps,const_x,const_y,coef_x,coef_y,temp_obstX,temp_obstY,radiusObst,constVelocity,tau );
        [ bigA,bigQ,bigC ] = changedAQ( steps,A,Q,C,tempA,tempB,lambda_vel,penalty_vel,numberObst);
        headerdl = cumsum([initialOmega;guess*deltaT]);
        headerd = headerdl(2:end,:);
        
        cvx_begin quiet
        variables dummy_plot_vel((numberObst+1)*(steps-1),1);
        minimize (((dummy_plot_vel'*bigA*dummy_plot_vel) + bigQ'*dummy_plot_vel)+bigC)
        subject to
        % constraints
        vel_max >= dummy_plot_vel(1:steps-1,:);
        dummy_plot_vel >= 0;
        (dummy_plot_vel(2:steps-1,:) - dummy_plot_vel(1:steps-2,:)) >= -accel_max*(deltaT);
        (dummy_plot_vel(2:steps-1,:) - dummy_plot_vel(1:steps-2,:)) <= accel_max*(deltaT);
        dummy_plot_vel(1:steps-1,:) >= radMin*abs(headerd);
        cvx_end
        [ trustDelta,constVelocity,oldConstraint,newConstraint,mu,nu ] = basicTrustRegionVel(constVelocity,steps,dummy_plot_vel,deltaT,guess,initialX,initialY,initialAng,initialOmega,finalX,finalY,finalAng,temp_obstX,temp_obstY,radiusObst,trustDelta,mu,nu,prev_optval_vel,cvx_optval,tau);
        mixed_vel = 0.01;
        %         figure(2);
        %         plot(constVelocity,'r');
        %         pause(2);
        
        if(numberObst >= 1)
            forceIndia = ((sum(repmat(double(dummy_plot_vel(1:steps-1,:)),1,numberObst*(steps-1)).*tempA ,1))' - tempB - (dummy_plot_vel(steps:end,:)));
            lambda_vel = lambda_vel+ penalty_vel.*forceIndia;
            for p = 1:numberObst
                if ((min(newConstraint{p,:}) < 0) && (abs(min(newConstraint{p,:}))) > 0.01)
                    %                 if(penalty_vel(((p-1)*(steps-1))+1,:) <10000000)
                    penalty_vel(((p-1)*(steps-1))+1:((p)*(steps-1)),:) = penalty_vel(((p-1)*(steps-1))+1:((p)*(steps-1)),:)*10;
                    %                 end
                else
                    % do nothing
                end
                if((abs(min(newConstraint{p,:}))) > mixed_vel && (min(newConstraint{p,:}))<0)
                    mixed_vel = (abs(min(newConstraint{p,:})));
                end
            end
        end
        if(abs(cvx_optval - prev_optval_vel) <= 5*10^-2 && mixed_vel < 0.011)
            counter_vel = counter_vel +1;
        else
            counter_vel = 0;
        end
        prev_optval_vel = cvx_optval;
        % debug
        disp('CVX_optval_velocity')
        disp(double(cvx_optval));
        disp('Status')
        disp(cvx_status);
    end
    % debug
    
    %     figure(2);
    %     hold on;
    %     loadeupplot
    %     pause(0.1);
    %     hold off;
    if((counter_alpha >= 1 && counter_vel >=1) || mainiter == cvxIterations)
        break;
    end
    mainiter = mainiter + 1;
    cla;
    trigger = 0;
    %   static senario
    %         plotOtimalPath( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger);
    %     dynamic senario
    %     visualization( steps,resultant_alpha,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger);
    [~,~] = plotOtimalPathDynamicRectangle( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger,circlesorpolygons,len,wid,obstth,obstLen,obstWid, trust_val,flagger);
    pause(0.1);
    %     for e = 1:steps-1
    %         angh = 0:0.001:2*pi;
    %         fill(temp_obstX(e,1)+(radiusObst(e,1)*cos(angh)),temp_obstY(e,1)+(radiusObst(e,1)*sin(angh)),'r')
    %         pause(2);
    %     end
    disp('-------------------------------------------------');
end
toc
disp('-------------------------------------------------');
disp('Total number of iterations :');
disp(mainiter);
disp('-------------------------------------------------');
disp('End of simulation');
constVelocityAlt = constVelocity;
guessAlt = guess;
headerdAlt = headerd;
save('variablesAlt.mat','constVelocityAlt','guessAlt','headerdAlt');
trigger = 1;
flagger = 1;
% plotOtimalPath( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger);
% resultant_alpha = double(dummy_plot_alpha(1:steps-1,:));
% plotOtimalPathDynamic( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius);
[finalx,finaly] = plotOtimalPathDynamicRectangle( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger,circlesorpolygons,len,wid,obstth,obstLen,obstWid,trust_val, 1);
plot(finalx,finaly,'b');
str = ['pic_next',num2str(trust_val),'.png'];
saveas(gcf,str);
disp('saved image');