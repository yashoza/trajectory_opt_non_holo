clear;
clc;
figure(1);
hold on;

X = linspace(1,30,100)';
Y = zeros(size(X));
Ang = zeros(size(X));
for t = 1:size(X,1)
    cla;
    axis([X(t,:)-5,X(t,:)+5,Y(t,:)-5,Y(t,:)+5])
    lengt = 3;
    width = 2;
    anglo = -Ang;
    micky = repmat([X(t,:);Y(t,:)],1,4)+([cos(anglo(t,:)),sin(anglo(t,:));-sin(anglo(t,:)),cos(anglo(t,:))]*[(lengt/2),(lengt/2),-(lengt/2),-(lengt/2);(width/2),-(width/2),-(width/2),(width/2)]);
    fill(micky(1,:),micky(2,:),'b');
    pause(0.1);
end
