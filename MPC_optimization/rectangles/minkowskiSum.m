function [ F ] = minkowskiSum( A,B )
%MINKSUM Summary of this function goes here
%   Detailed explanation goes here


szA=size(A); szB=size(B);
S=zeros(szA(1)*szB(1),szA(2));
for k=1:szA(2)
   temp=bsxfun(@plus,A(:,k),B(:,k)');
   S(:,k)=temp(:);
end

[P,~,~]=unique(S,'rows');
i = convhull(P);
F = P(i,:);

end

