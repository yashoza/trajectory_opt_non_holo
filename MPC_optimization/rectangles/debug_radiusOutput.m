function [radiusObst,temp_obstX,temp_obstY] = radiusOutput(steps,obstX,obstY,obstth,obstLen,obstWid,const_x,const_y,const_th,len,wid,initialX,initialY,finalX,finalY)
%RADIUSOUTPUT Summary of this function goes here
%   Detailed explanation goes here

radiusObst = zeros(size(obstX));
temp_obstX = zeros(size(obstX));
temp_obstY = zeros(size(obstX));
figure(3);
hold on;
set(gca,'XTick',[])
set(gca,'YTick',[])
% axis ([-7 7 -7 -2]);
axis equal;
length = steps+2;
red = [30,144,255]/255;
pink = [0,0,255]/255;
colors_p = [linspace(red(1),pink(1),length)', linspace(red(2),pink(2),length)', linspace(red(3),pink(3),length)'];

for b = 1:size(obstX,2)
    dipx = obstX(:,b);
    dipy = obstY(:,b);
    dipth = - obstth(:,b);
    for c = 1:steps-1
        lengt = obstLen(:,b);
        width = obstWid(:,b);
        th = dipth(c,:);
        tempObst = repmat([dipx(c,:);dipy(c,:)],1,4)+([cos(th),sin(th);-sin(th),cos(th)]*[(lengt/2),(lengt/2),-(lengt/2),-(lengt/2);(width/2),-(width/2),-(width/2),(width/2)]);
        lengt = len;
        width = wid;
        th = - const_th(c,:);
        tempRobo = repmat([const_x(c,:);const_y(c,:)],1,4) + ([cos(th),sin(th);-sin(th),cos(th)]*[(lengt/2),(lengt/2),-(lengt/2),-(lengt/2);(width/2),-(width/2),-(width/2),(width/2)]);
        cla;
        plot([-7,7],[-6,-6],'k','LineWidth',1.5);
        plot([-7,7],[-2,-2],'k','LineWidth',1.5);
        plot([-7,7],[-4,-4],'k--','LineWidth',1.2);
        plot(const_x,const_y,'b');
        if(checkCollide(tempObst',tempRobo') == 0)
            lift = minkowskiSum( tempRobo',tempObst' );
            hoax = zeros(size(lift));
            hoax(:,1) = lift(:,1) - const_x(c,:);
            hoax(:,2) = lift(:,2) - const_y(c,:);
%             ang = zeros(size(hoax,1),1);
            kool = zeros(size(hoax,1),size(hoax,1));
            for n = 1:size(hoax,1)
                for g = 1:size(hoax,1)
                    dotab = (hoax(g,1) - const_x(c,:))*(hoax(n,1)-const_x(c,:)) + (hoax(g,2) - const_y(c,:))*(hoax(n,2)-const_y(c,:));
                    moda = sqrt((hoax(g,1) - const_x(c,:)).^2 + (hoax(g,2) - const_y(c,:)).^2);
                    modb = sqrt((hoax(n,2)-const_y(c,:)).^2 + (hoax(n,1)-const_x(c,:)).^2);
                    kool(g,n) = acos(dotab/(moda*modb));
                end
            end
            plot(hoax(:,1),hoax(:,2),'k');
            hatch(patch(hoax(:,1),hoax(:,2),'w'),45,[0 0 0],'-',3,1);
            for hol = 1:c
                th = - const_th(hol,:);
                lengt = len;
                width = wid;
                milaRobo = repmat([const_x(hol,:);const_y(hol,:)],1,4) + ([cos(th),sin(th);-sin(th),cos(th)]*[(lengt/2),(lengt/2),-(lengt/2),-(lengt/2);(width/2),-(width/2),-(width/2),(width/2)]);
                fill(milaRobo(1,:),milaRobo(2,:),colors_p(hol,:));
            end
            fill(tempObst(1,:),tempObst(2,:),'r');
%             plot(initialX,initialY,'bO');
% % %             plot(finalX,finalY,'bO');
            [mili,zip1] = max(kool);
            [maxi,pop2] = max(mili);
            pop1 = zip1(1,pop2);
            angle = maxi;
            %             angle = asin((((hoax(pop1,1)-const_x(c,:))*(hoax(pop2,2)-const_y(c,:))) - ((hoax(pop1,2)-const_y(c,:))*(hoax(pop2,1)-const_x(c,:))))/((sqrt((hoax(pop1,1)-const_x(c,:)).^2 + (hoax(pop1,2)-const_y(c,:)).^2))*(sqrt((hoax(pop2,1)-const_x(c,:)).^2 + (hoax(pop2,2)-const_y(c,:)).^2))));
            tuff = [sqrt((const_x(c,:) - hoax(pop1,1)).^2 + (const_y(c,:) - hoax(pop1,2)).^2),sqrt((const_x(c,:) - hoax(pop2,1)).^2 + (const_y(c,:) - hoax(pop2,2)).^2)];
            pumba = min(tuff);
            radiusObst(c,b) = pumba*tan(abs(angle)/2);
            u1 = [hoax(pop1,1) - const_x(c,:),hoax(pop1,2) - const_y(c,:)];
            u2 = [hoax(pop2,1) - const_x(c,:),hoax(pop2,2) - const_y(c,:)];
            sumu = (u1/norm(u1)) + (u2/norm(u2));
            sumu = sumu/(norm(sumu));
            temp_obstX(c,b) = const_x(c,:)+ (pumba/(cos(abs(angle)/2)))*sumu(1,1);
            temp_obstY(c,b) = const_y(c,:)+ (pumba/(cos(abs(angle)/2)))*sumu(1,2);
            plot([const_x(c,:),const_x(c,:)+ (pumba/(cos(abs(angle)/2)))*sumu(1,1)],[const_y(c,:),const_y(c,:)+ (pumba/(cos(abs(angle)/2)))*sumu(1,2)],'b','LineWidth',1.5);
            plot([const_x(c,:),hoax(pop1,1)],[const_y(c,:),hoax(pop1,2)],'b','LineWidth',1.5);
            plot([const_x(c,:),hoax(pop2,1)],[const_y(c,:),hoax(pop2,2)],'b','LineWidth',1.5);
            angh = 0:0.001:2*pi;
            plot(temp_obstX(c,b)+(radiusObst(c,b)*cos(angh)),temp_obstY(c,b)+(radiusObst(c,b)*sin(angh)),'k','LineWidth',1.5)
            print(sprintf('Images/Minkowski_%d',c), '-dpng', '-r300');
        else
            radiusObst(c,b) = (sqrt((len/2)^2 + (wid/2).^2) + sqrt((obstLen(:,b)/2)^2 + (obstWid(:,b)/2)^2));
            temp_obstX(c,b) = obstX(c,b);
            temp_obstY(c,b) = obstY(c,b);
        end
    end
    
    %    close(3);
end

