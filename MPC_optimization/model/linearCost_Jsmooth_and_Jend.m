function [ A,Q,C,coef_x,coef_y ] = linearCost_Jsmooth_and_Jend( steps,deltaT,guess,initialAng,initialOmega,finalAng,half_mat,rev_half_mat,sin_total_vel,cos_total_vel,const_x,const_y,finalX,finalY )
%LINEARCOST Summary of this function goes here
%   Detailed explanation goes here

utmatrix = triu(ones(steps-1,steps-1),0);
coef_x = cell(steps-1,1);
coef_y = cell(steps-1,1);
coef_th = cell(steps-1,1);
lin_x = cell(steps-1,1);
lin_y = cell(steps-1,1);
f_const = zeros(steps-1,1);
mist_th = zeros(size(guess));

%% Calculation of A,Q,C from (xn-xg).^2 and (yn-yg).^2 
for m = 1:steps-1
    temp_utmatrix = utmatrix(1:m,1:m);
    multi_mat = temp_utmatrix .* repmat((rev_half_mat(1:m))',m,1);
    for y = 1:size(multi_mat,1)
        for k = 1:size(multi_mat,2)
            if(multi_mat(y,k) > 0 )
                multi_mat(y,k) = multi_mat(y,k) - ((y-1)*deltaT*deltaT);
            end
        end
    end
    out_x = -1*multi_mat*sin_total_vel(1:m,:);
    out_y = multi_mat*cos_total_vel(1:m,:);
    coef_x{m,1} = [out_x;zeros(steps-m-1,1)];
    lx = (2*(const_x(m+1,:)+sum(out_x.*(-1*guess(1:m,:)))-finalX));
    lin_x{m,1} = [(lx*out_x);zeros(steps-m-1,1)];
    coef_y{m,1} = [out_y;zeros(steps-m-1,1)];
    ly = (2*(const_y(m+1,:)+sum(out_y.*(-1*guess(1:m,:)))-finalY));
    lin_y{m,1} = [(ly*out_y);zeros(steps-m-1,1)];
    f_const(m,1) = (lx*lx + ly*ly)/4;
end
A = (((coef_x{end,1})*(coef_x{end,1})') + ((coef_y{end,1})*(coef_y{end,1})')) + (eye(size(guess,1),size(guess,1)));
Q = (lin_x{end,:}+lin_y{end,:})';

    %% Calculation of A,Q,C from (thn-thg).^2
    
    for y = 1:size(guess,1)
        coef_th{y,1} = [half_mat(steps-y:end,:);zeros(steps-y-1,1)];
        mist_th(y,:) = initialAng + (y*initialOmega*deltaT) - finalAng;
        f_const(y,:) = f_const(y,:) + 4*mist_th(y,:)*mist_th(y,:);
    end
    A = A + (coef_th{end,:}*(coef_th{end,:})');
    Q = Q + (2*mist_th(end,:)*coef_th{end,:})';
    C = f_const(end,:);
    
end

