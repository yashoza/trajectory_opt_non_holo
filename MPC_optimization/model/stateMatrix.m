function [half_mat,rev_half_mat,sin_total_vel,cos_total_vel,total_ang,const_x,const_y] = stateMatrix(steps,alpha,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega)
%STATEMATRIX Computes state at particular Alpha
%   calulates state of matrix at particular alpha
%   creates half_mat, rev_half_mat, sin_total_vel , cos_total_vel ,
%   total_ang for later use

% Matrix Initilization
half_mat = zeros(size(alpha));  % coefficients of alpha terms in X,Y  
% //example : third angle = initialAngle + 3*initialOmega*deltaT + (5/2)*(deltaT)^2*(alpha(0)) + (3/2)*(deltaT)^2*(alpha(1)) + (1/2)*(deltaT)^2*(alpha(3)) //
% Here 1/2,3/2,5/2 are last three therms of half_mat

for h = 1:steps-1
    half_mat(h,:) = (steps-h-0.5)*deltaT*deltaT;
end
rev_half_mat = half_mat(end:-1:1); % half_mat reverse in order

total_ang = zeros(steps-1,1);
for m = 1:steps-1
    total_ang(m,:) = initialAng+(m*initialOmega*deltaT)+((alpha(1:m,:))'*half_mat(steps-m:end,:)); % angle at every step
end

sin_total_vel = (constVelocity .* sin(total_ang))*deltaT; % change in y-coordinate by the end of step //example : y = y + v*sin(thirdangle)*deltaT //
cos_total_vel = (constVelocity .* cos(total_ang))*deltaT;

const_x = cumsum([initialX;cos_total_vel]); % X-coordinate of robot
const_y = cumsum([initialY;sin_total_vel]); % Y-coordinates of robot

end

