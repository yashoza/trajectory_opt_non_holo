function [ tempA,tempB ] = augmentedLagrangianVelocity( steps,const_x,const_y,coef_x,coef_y,obstX,obstY,radiusObst,guess_velocity,tau )
%AUGMENTEDLAGRANGIAN Summary of this function goes here
%   Detailed explanation goes here

%% Calculation of tempA,tempB from Jobst
tempA = [];
tempB = [];

for g = 1:size(obstX,2)
    const_obst = ((const_x(2:end,:)-obstX(:,g)).^2 + (const_y(2:end,:)-obstY(:,g)).^2);
    lin_obst = zeros(steps-1,steps-1);
    f_const_obst = zeros(steps-1,1);
    if (size(obstX,1) == steps-1)
        fixX = obstX(:,g);
        fixY = obstY(:,g);
        for m = 1:steps-1
            lin_obst(:,m) = ((2*(const_x(m+1,:)-fixX(m,:))*(coef_x{m,:}))+(2*(const_y(m+1,:)-fixY(m,:))*(coef_y{m,:})));
            f_const_obst(m,:) = ((2*(const_x(m+1,:)-fixX(m,:))*((coef_x{m,:})'*(-1*guess_velocity)))+(2*(const_y(m+1,:)-fixY(m,:))*((coef_y{m,:})'*(-1*guess_velocity))));
        end
    else
        for m = 1:steps-1
            lin_obst(:,m) = ((2*(const_x(m+1,:)-obstX(:,g))*(coef_x{m,:}))+(2*(const_y(m+1,:)-obstY(:,g))*(coef_y{m,:})));
            f_const_obst(m,:) = ((2*(const_x(m+1,:)-obstX(:,g))*((coef_x{m,:})'*(-1*guess_velocity)))+(2*(const_y(m+1,:)-obstY(:,g))*((coef_y{m,:})'*(-1*guess_velocity))));
        end
    end
    memA = lin_obst;
    memB = ((radiusObst(:,g) + tau).^2 - const_obst - f_const_obst);
    % one way of linearization
    for bill = 1:steps-1
        maxil = max(abs([memA(:,bill);memB(bill,:)]));
        memA(:,bill) = memA(:,bill)/(bill*maxil);
        memB(bill,:) = memB(bill,:)/(bill*maxil);
    end
%     % another way of linearization
%     maxim = max(abs(memB));
%     memA = memA/maxim*0.01;
%     memB = memB/maxim*0.01;
%     for bill = 1:steps-1
%         memA(:,bill) = memA(:,bill)/(steps-bill);
%         memB(bill,:) = memB(bill,:)/(steps-bill);
%     end
    tempA = [tempA,memA];
    tempB = [tempB;memB];
end
end

