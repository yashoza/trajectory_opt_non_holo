function [ A,Q,C,coef_x,coef_y ] = linearCost_Jsmooth_and_Jend_velocity( steps,sin_total_vel,cos_total_vel,initialX,initialY,finalX,finalY,constVelocity,initialVel,finalVel,deltaT )
%LINEARCOST Summary of this function goes here
%   Detailed explanation goes here

coef_x = cell(steps-1,1);
coef_y = cell(steps-1,1);
lin_x = cell(steps-1,1);
lin_y = cell(steps-1,1);
f_const = zeros(steps-1,1);

cos_term = cos_total_vel ./ constVelocity;
sin_term = sin_total_vel ./ constVelocity;
%% Calculation of A,Q,C from (xn-xg).^2 and (yn-yg).^2
for m = 1:steps-1
    out_x = cos_term(1:m,:);
    out_y = sin_term(1:m,:);
    coef_x{m,1} = [out_x;zeros(steps-m-1,1)];
    lx = (2*(initialX-finalX));
    lin_x{m,1} = [(lx*out_x);zeros(steps-m-1,1)];
    coef_y{m,1} = [out_y;zeros(steps-m-1,1)];
    ly = (2*(initialY-finalY));
    lin_y{m,1} = [(ly*out_y);zeros(steps-m-1,1)];
    f_const(m,1) = (lx*lx + ly*ly)/4;
end
A = (((coef_x{end,1})*(coef_x{end,1})') + ((coef_y{end,1})*(coef_y{end,1})'));
Q = (lin_x{end,:}+lin_y{end,:})';
C = f_const(end,:);

%% final velocity

A(1,1) = A(1,1) + 40;
A(end,end) = A(end,end) + 1;
Q(1,1) = Q(1,1) -80*initialVel;
Q(1,end) = Q(1,end) -2*finalVel;
C = C + 40*(initialVel)^2 + (finalVel)^2;

%% Sum of jerk and accelration
d0 = -1*ones(steps-2,1);
d1 = ones(steps-3,1);
fndfA = diag(d0) + diag(d1,1);
fndfB = [fndfA,zeros(steps-2,1)];
fndfB(end,end) = 1;
fndfC = (fndfB'*fndfB)/(deltaT^2);
A = A + fndfC;
d0 = ones(steps-3,1);
d1 = -2*ones(steps-4,1);
d2 = ones(steps-5,1);
fndfA = diag(d0) + diag(d1,1) + diag(d2,2);
fndfB = [fndfA,zeros(steps-3,1),zeros(steps-3,1)];
fndfB(end,end) = 1;
fndfB(end,end-1) = -2;
fndfB(end-1,end-1) = 1;
fndfC = (fndfB'*fndfB)/(deltaT^4);
A = A + fndfC;
end

