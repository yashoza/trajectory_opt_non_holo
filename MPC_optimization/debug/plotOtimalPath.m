function plotOtimalPath( steps,resultant_alpha,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger)
%PLOTOTIMALPATH Summary of this function goes here
%   Detailed explanation goes here
% myVideo = VideoWriter('basic_bernstein_path.avi','Uncompressed AVI');
% open(myVideo);
figure(1);
axis([-6 6 -6 6]);
hold on;
[~,~,~,~,~,final_x,final_y] = stateMatrix(steps,resultant_alpha,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);
anglo = 0:0.001:2*pi;
if (trigger == 0)
    plot(final_x,final_y,'b');
    plot(initialX,initialY,'bO');
    plot(finalX,finalY,'bO');
    for i =1:size(obstX,2)
        fill(obstX(:,i) + ((radiusObst(:,i))*cos(anglo)),obstY(:,i) + ((radiusObst(:,i))*sin(anglo)),'r');
    end
else
    for h = 1:steps-1
        cla;
        plot(initialX,initialY,'bO');
        plot(finalX,finalY,'bO');
        for i =1:size(obstX,2)
            fill(obstX(:,i) + ((radiusObst(:,i) - mainRadius)*cos(anglo)),obstY(:,i) + ((radiusObst(:,i)-mainRadius)*sin(anglo)),'r');
        end
        plot(final_x,final_y,'b');
        fill(final_x(h+1,:) + mainRadius*cos(anglo),final_y(h+1,:) + mainRadius*sin(anglo),'b');
        pause(0.01);
%             F = getframe;
%             writeVideo(myVideo,F);
    end
end
% close(myVideo);
end

