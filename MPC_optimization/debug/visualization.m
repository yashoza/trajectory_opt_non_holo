function visualization( steps,resultant_alpha,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius)
%PLOTOPTIMALPATHDYNAMIC Summary of this function goes here
%   Detailed explanation goes here
% myVideo = VideoWriter('dynamic_obstrcles4.avi','Uncompressed AVI');
% open(myVideo);
figure(1);
axis([-3 15 -3 15]);
hold on;
[~,~,~,~,~,final_x,final_y] = stateMatrix(steps,resultant_alpha,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);
anglo = 0:0.01:2*pi;

for t = 1:steps-1
    cla;
    plot(initialX,initialY,'bO');
    plot(finalX,finalY,'bO');
    for i =1:size(obstX,2)
        fixX = obstX(:,i);
        fixY = obstY(:,i);
        for u = 1:steps-1
            fill(fixX(u,:) + (radiusObst(:,i)-mainRadius)*cos(anglo),fixY(u,:) + (radiusObst(:,i)-mainRadius)*sin(anglo),'r');
        end
    end
    plot(final_x,final_y,'b');
    fill(final_x(t+1,:) + mainRadius*cos(anglo),final_y(t+1,:) + mainRadius*sin(anglo),'b');
    F = getframe;
%     writeVideo(myVideo,F);
%     pause(0.1);
end
% close(myVideo);
end
