function plotOtimalPathDynamic( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius)
%PLOTOPTIMALPATHDYNAMIC Summary of this function goes here
%   Detailed explanation goes here
myVideo = VideoWriter('random.avi','Uncompressed AVI');
open(myVideo);
figure(1);
axis([-6 6 -6 6]);
hold on;
[~,~,~,~,~,final_x,final_y] = stateMatrix(steps,guess,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);
anglo = 0:0.01:2*pi;

for t = 1:steps-1
    cla;
    plot(initialX,initialY,'bO');
    plot(finalX,finalY,'bO');
    for i =1:size(obstX,2)
        fixX = obstX(:,i);
        fixY = obstY(:,i);
        plot(fixX,fixY,'r');
        fill(fixX(t,:) + (radiusObst(:,i)-mainRadius)*cos(anglo),fixY(t,:) + (radiusObst(:,i)-mainRadius)*sin(anglo),'r');
    end
    plot(final_x,final_y,'b');
    fill(final_x(t+1,:) + mainRadius*cos(anglo),final_y(t+1,:) + mainRadius*sin(anglo),'b');
    F = getframe;
    writeVideo(myVideo,F);
    pause(0.001);
end
close(myVideo);
end

