function [final_x,final_y] = plotOtimalPathDynamicRectangle( steps,guess,initialX,initialY,initialAng,initialOmega,constVelocity,deltaT,obstX,obstY,radiusObst,finalX,finalY,mainRadius,trigger,circlesorpolygons,len,wid,obstth,obstlen,obstwid,val, flagger)
%PLOTOPTIMALPATHDYNAMIC Summary of this function goes here
%   Detailed explanation goes here
myVideo = VideoWriter('vel_exploit_obst_alt_vel3.avi','Uncompressed AVI');
open(myVideo);
figure(1);
axis([-9 9 -9 9]);
hold on;
[~,~,~,~,final_th,final_x,final_y] = stateMatrix(steps,guess,deltaT,constVelocity,initialX,initialY,initialAng,initialOmega);

if(trigger == 1)
    if(circlesorpolygons == 0)
        anglo = 0:0.01:2*pi;
        for t = 1:steps-1
            cla;
            plot(initialX,initialY,'bO');
            plot(finalX,finalY,'bO');
            for i =1:size(obstX,2)
                fixX = obstX(:,i);
                fixY = obstY(:,i);
                plot(fixX,fixY,'r');
                fill(fixX(t,:) + (radiusObst(:,i)-mainRadius)*cos(anglo),fixY(t,:) + (radiusObst(:,i)-mainRadius)*sin(anglo),'r');
            end
            plot(final_x,final_y,'b');
            fill(final_x(t+1,:) + mainRadius*cos(anglo),final_y(t+1,:) + mainRadius*sin(anglo),'b');
            %     F = getframe;
            %     writeVideo(myVideo,F);
            pause(0.001);
        end
    else
        for t = 1:steps-1
            cla;
            plot(initialX,initialY,'bO');
            plot(finalX,finalY,'bO');
            plot(final_x,final_y,'b');
            for i =1:size(obstX,2)
                fixX = obstX(:,i);
                fixY = obstY(:,i);
                polly = -obstth(:,i);
                angXY = polly(t,:);
                lengt = obstlen(:,i);
                width = obstwid(:,i);
                plot(fixX,fixY,'r');
                tempObst = repmat([fixX(t,:);fixY(t,:)],1,4)+([cos(angXY),sin(angXY);-sin(angXY),cos(angXY)]*[(lengt/2),(lengt/2),-(lengt/2),-(lengt/2);(width/2),-(width/2),-(width/2),(width/2)]);
                fill(tempObst(1,:),tempObst(2,:),'r');
            end
            lengt = len;
            width = wid;
            anglo = -final_th;
            micky = repmat([final_x(t,:);final_y(t,:)],1,4)+([cos(anglo(t,:)),sin(anglo(t,:));-sin(anglo(t,:)),cos(anglo(t,:))]*[(lengt/2),(lengt/2),-(lengt/2),-(lengt/2);(width/2),-(width/2),-(width/2),(width/2)]);
            fill(micky(1,:),micky(2,:),'b');
            %
            F = getframe;
            writeVideo(myVideo,F);
            pause(0.1);
        end
    end
else
    if(circlesorpolygons == 0)
        %         cla;
        plot(initialX,initialY,'bO');
        plot(finalX,finalY,'bO');
        for i =1:size(obstX,2)
            fixX = obstX(:,i);
            fixY = obstY(:,i);
            plot(fixX,fixY,'r');
        end
        plot(final_x,final_y,'b');
        %     F = getframe;
        %     writeVideo(myVideo,F);
    else
        cla;
        plot(initialX,initialY,'bO');
        plot(finalX,finalY,'bO');
        %         plot([-6,-2],[5.5,5.5],'k--');
        %         plot([2,6],[5.5,5.5],'k--');
        %         plot([0,0],[-1,3],'k--');
        for i =1:size(obstX,2)
            fixX = obstX(:,i);
            fixY = obstY(:,i);
            holp = -obstth(:,i);
            angXY = holp(1,:);
            lengt = obstlen(:,i);
            width = obstwid(:,i);
            plot(fixX,fixY,'r');
            tempObst = repmat([fixX(1,:);fixY(1,:)],1,4)+([cos(angXY),sin(angXY);-sin(angXY),cos(angXY)]*[(lengt/2),(lengt/2),-(lengt/2),-(lengt/2);(width/2),-(width/2),-(width/2),(width/2)]);
            fill(tempObst(1,:),tempObst(2,:),'r');
        end
        
        plot(final_x,final_y,'b');
      
        
        %F = getframe;
        %                 writeVideo(myVideo,F);
    end
end
close(myVideo);
end

